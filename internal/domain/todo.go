package domain

type Todo struct {
	Id       int64  `json:"id"`
	Name     string `json:"name"`
	Complete bool   `json:"complete"`
}
